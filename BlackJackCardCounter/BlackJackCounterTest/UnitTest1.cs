using NUnit.Framework;
using System.Collections.Generic;
using BlackJackCardCounter;

namespace BlackJackCounterTest
{
    [TestFixture]
    public class CardGameTests
    {
        private IDeck mainDeck;
        private ICardCounter mainCardCounter;
        private ICardValidator cardValidator;

        [SetUp]
        public void Setup()
        {
            mainDeck = new Deck();
            mainCardCounter = new CardCounter(mainDeck);
            cardValidator = new CardValidator();
        }


        [Test]
        public void CardCounterControlTest()
        {
            string input = "5 of Hearts";

            bool isValid = cardValidator.IsValidCardFormat(input);

            Assert.IsTrue(isValid);
        }


        //Old Test was here for some old code, removed it because it was irrelevant and I never showed it in the gui

        [Test]
        public void ardCounterBadInputFinder1()
        {
            string invalidInput = "Not Real Card";

            bool isValid = cardValidator.IsValidCardFormat(invalidInput);

            Assert.IsFalse(isValid);
        }
        [Test]
        public void CardCounterBadInputFinder2()
        {
            string invalidInput = "hearts of 2";

            bool isValid = cardValidator.IsValidCardFormat(invalidInput);

            Assert.IsFalse(isValid);
        }
    }
}