﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackCardCounter
{
    public interface ICardValidator
    {
        bool IsValidCardFormat(string input);
        bool IsValidCardForPlayer(Card card, List<Card> spentCards);
        bool IsValidCardForDealer(Card card, List<Card> spentCards);
    }
}
