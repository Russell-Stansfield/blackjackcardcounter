﻿namespace BlackJackCardCounter
{
    public class Program
    {
        private static IDeck mainDeck;
        private static ICardCounter mainCardCounter;
        private static ICardValidator cardValidator;

        public static void Main(string[] args)
        {
            Console.Write("Enter the number of decks: ");
            int numberOfDecks;
            if (int.TryParse(Console.ReadLine(), out numberOfDecks) && numberOfDecks > 0)
            {
                mainDeck = new Deck(numberOfDecks);
                mainCardCounter = new CardCounter(mainDeck);
                cardValidator = new CardValidator();
                bool isProgramRunning = true;

                Console.WriteLine("Welcome to the Card Counting Program!");

                while (isProgramRunning)
                {
                    Console.WriteLine("\n1. Enter a card for the player's hand");
                    Console.WriteLine("2. Enter a card for the dealer's hand");
                    Console.WriteLine("3. Show remaining cards in the deck");
                    Console.WriteLine("4. Show spent cards");
                    Console.WriteLine("5. Show card count");
                    Console.WriteLine("6. Show percentages for each card value");
                    Console.WriteLine("7. Show Percentage for Card Value");
                    Console.WriteLine("8. Exit");
                    Console.Write("Choose an option: ");
                    int option;
                    if (int.TryParse(Console.ReadLine(), out option))
                    {
                        switch (option)
                        {
                            case 1:
                                EnterCard("player");
                                break;
                            case 2:
                                EnterCard("dealer");
                                break;
                            case 3:
                                ShowRemainingCards();
                                break;
                            case 4:
                                ShowSpentCards();
                                break;
                            case 5:
                                ShowCardCount();
                                break;
                            case 6:
                                ShowPercentagesForEachCardValue();
                                break;
                            case 7:
                                ShowPercentageForCardValue();
                                break;
                            case 8:
                                isProgramRunning = false;
                                break;
                            default:
                                Console.WriteLine("Invalid option. Please choose again.");
                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid input. Please enter a valid integer option.");
                    }
                }
            }
            else
            {
                Console.WriteLine("Invalid number of decks. Please enter a positive integer.");
            }
        }

        private static void ShowPercentagesForEachCardValue()
        {
            Dictionary<string, double> percentages = mainDeck.GetPercentagesForEachCardValue();

            Console.WriteLine("\nPercentages for each card value:");
            foreach (var kvp in percentages)
            {
                Console.WriteLine($"{kvp.Key}: {kvp.Value}%");
            }
        }
        private static void ShowRemainingCards()
        {
            List<Card> remainingCards = mainDeck.GetRemainingCards();

            if (remainingCards.Count > 0)
            {
                Console.WriteLine("\nRemaining cards in the deck:");
                foreach (Card card in remainingCards)
                {
                    Console.WriteLine($"{card.Rank} of {card.Suit}");
                }
            }
            else
            {
                Console.WriteLine("No remaining cards in the deck.");
            }
        }

        private static void EnterCard(string player)
        {
            Console.Write($"Enter the card for the {player} (ex: '5 of Hearts'): ");
            string input = Console.ReadLine();

            if (cardValidator.IsValidCardFormat(input))
            {
                string[] parts = input.Split(" ");
                string rank = parts[0];
                string suit = parts[2];

                Card card = new Card(suit, rank);
                List<Card> spentCards = mainDeck.GetSpentCards();

                // Check if the card is already in the player's or dealer's hand
                List<Card> currentHand = player == "player" ? mainDeck.GetPlayerHand() : mainDeck.GetDealerHand();  //might be issue here
                if (!currentHand.Contains(card) && cardValidator.IsValidCardForPlayer(card, spentCards))
                {
                    mainDeck.RemoveCard(card);
                    Console.WriteLine($"Added {card.Rank} of {card.Suit} to the {player}'s hand.");
                }
                else
                {
                    if (currentHand == null)
                    {
                        Console.WriteLine($"Error: {player} hand is null.");
                    }
                    else if (currentHand.Contains(card))
                    {
                        Console.WriteLine($"Error: {player}'s hand already contains {card.Rank} of {card.Suit}.");
                    }
                    else if (!cardValidator.IsValidCardForPlayer(card, spentCards))
                    {
                        Console.WriteLine($"Error: Invalid card format or card already spent.");
                    }
                    else
                    {
                        Console.WriteLine($"Unknown error occurred.");
                    }
                }
            }
            else
            {
                Console.WriteLine("Invalid input format. Please try again.");
            }
        }



        private static void ShowSpentCards()
        {
            List<Card> spentCards = mainDeck.GetSpentCards();
            Console.WriteLine("\nSpent cards:");
            foreach (Card card in spentCards)
            {
                Console.WriteLine($"{card.Rank} of {card.Suit}");
            }
        }

        private static void ShowCardCount()
        {
            Dictionary<string, int> cardCount = mainCardCounter.GetCardCount();
            Console.WriteLine("\nCard count:");
            foreach (var kvp in cardCount)
            {
                Console.WriteLine($"{kvp.Key}: {kvp.Value}");
            }
        }
        private static void ShowPercentageForCardValue()
        {
            Console.Write("Enter the card value (e.g., 2, 3, A): ");
            string value = Console.ReadLine().ToUpper();

            if (value == "10") // Special case for 10
            {
                value = "T";
            }

            double percentage = mainDeck.GetPercentageForCardValue(value);
            Console.WriteLine($"The percentage of getting a {value} is: {percentage}%");
        }
    }
}