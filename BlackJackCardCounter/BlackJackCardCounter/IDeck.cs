﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackCardCounter
{
    public interface IDeck
    {
        void RemoveCard(Card card);
        List<Card> GetRemainingCards();
        List<Card> GetSpentCards();
        void MarkCurrentHandAsSpent();
        double GetPercentageForCardValue(string value);
        Dictionary<string, double> GetPercentagesForEachCardValue();
        List<Card> GetPlayerHand();  //Might be redeclaring this each and everytime
        List<Card> GetDealerHand();
    }
}
