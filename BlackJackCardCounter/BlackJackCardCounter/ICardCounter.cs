﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackCardCounter
{
    public interface ICardCounter
    {
        Dictionary<string, int> GetCardCount();
    }
}
