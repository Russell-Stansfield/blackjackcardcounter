﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackCardCounter
{
    public class Deck : IDeck
    {
        private List<Card> cards;
        private SpentCards spentCards;
        private SpentCards totalSpentCards;
        private List<Card> playerHand;
        private List<Card> dealerHand;

        public Deck()
        {
            cards = GenerateDeck(numberOfDecks);
            spentCards = new SpentCards();
            totalSpentCards = new SpentCards();
            playerHand = new List<Card>();
            dealerHand = new List<Card>();
        }

        private List<Card> GenerateDeck(int numberOfDecks)
        {
            List<Card> deck = new List<Card>();
            string[] suits = { "Hearts", "Diamonds", "Clubs", "Spades" };
            string[] ranks = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };

            for (int i = 0; i < numberOfDecks; i++)
            {
                foreach (string suit in suits)
                {
                    foreach (string rank in ranks)
                    {
                        deck.Add(new Card(suit, rank));
                    }
                }
            }

            return deck;
        }
        public List<Card> GetPlayerHand()
        {
            if (playerHand == null)
            {
                playerHand = new List<Card>();
            }
            return playerHand;
        }

        public List<Card> GetDealerHand()
        {
            if (dealerHand == null)
            {
                dealerHand = new List<Card>();
            }
            return dealerHand;
        }

        public void RemoveCard(Card card)
        {
            cards.Remove(card);
            spentCards.AddCard(card);
        }

        public List<Card> GetRemainingCards()
        {
            return cards;
        }

        public List<Card> GetSpentCards()
        {
            return spentCards.GetSpentCards();
        }
        public void MarkCurrentHandAsSpent()
        {
            totalSpentCards.AddCardRange(spentCards.GetSpentCards());
            spentCards.Clear();
        }
        public double GetPercentageForCardValue(string value)
        {
            List<Card> remainingCards = GetRemainingCards();
            int cardsWithSpecifiedValue = remainingCards.Count(card => card.Rank == value);
            int totalRemainingCards = remainingCards.Count;

            return (double)cardsWithSpecifiedValue / totalRemainingCards * 100;
        }
        private int numberOfDecks;

        public Deck(int numberOfDecks)
        {
            this.numberOfDecks = numberOfDecks;
            cards = GenerateDeck(numberOfDecks);
            spentCards = new SpentCards();
            totalSpentCards = new SpentCards();
        }
        

        public Dictionary<string, double> GetPercentagesForEachCardValue()
        {
            List<Card> remainingCards = GetRemainingCards();
            Dictionary<string, int> cardCounts = remainingCards
                .GroupBy(card => card.Rank)
                .ToDictionary(group => group.Key, group => group.Count());

            Dictionary<string, double> percentages = new Dictionary<string, double>();

            foreach (var kvp in cardCounts)
            {
                percentages[kvp.Key] = (double)kvp.Value / (52 * numberOfDecks) * 100;
            }

            return percentages;
        }
    }
}
