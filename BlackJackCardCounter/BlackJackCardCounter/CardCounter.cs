﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackCardCounter
{
    public class CardCounter : ICardCounter
    {
        private IDeck deck;
        private Dictionary<string, int> cardCount;

        public CardCounter(IDeck deck)
        {
            this.deck = deck;
            cardCount = new Dictionary<string, int>();
            InitializeCardCount();
        }
        private void InitializeCardCount()
        {
            string[] ranks = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };

            foreach (string rank in ranks)
            {
                cardCount[rank] = 0;
            }
        }



        public Dictionary<string, int> GetCardCount()
        {
            return cardCount;
        }
    }
}
