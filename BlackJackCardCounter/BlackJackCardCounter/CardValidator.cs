﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackCardCounter
{
    public class CardValidator : ICardValidator
    {
        public bool IsValidCardFormat(string input)
        {
            // Checks if the input has the correct format (e.g., "5 of Hearts")
            string[] parts = input.Split(" ");
            if (parts.Length != 3)
            {
                return false;
            }

            // Checks for specific terms in the input
            string rank = parts[0].ToUpper();
            string suit = parts[2].ToUpper();
            string[] validSuits = { "HEARTS", "DIAMONDS", "CLUBS", "SPADES" };
            string[] validRanks = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };

            return validRanks.Contains(rank) && validSuits.Contains(suit);
        }

        public bool IsValidCardForPlayer(Card card, List<Card> spentCards)
        {
            return !spentCards.Contains(card);
        }

        public bool IsValidCardForDealer(Card card, List<Card> spentCards)
        {
            return !spentCards.Contains(card);
        }
    }

}
