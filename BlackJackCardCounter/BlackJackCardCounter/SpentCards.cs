﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackCardCounter
{
    public class SpentCards
    {
        private List<Card> spentCards;

        public SpentCards()
        {
            spentCards = new List<Card>();
        }

        public void AddCard(Card card)
        {
            spentCards.Add(card);
        }

        public void AddCardRange(List<Card> cards)
        {
            spentCards.AddRange(cards);
        }

        public List<Card> GetSpentCards()
        {
            return spentCards;
        }

        public void Clear()
        {
            spentCards.Clear();
        }
    }
}
